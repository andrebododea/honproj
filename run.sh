#!/bin/bash

# if build directory exists
if [ -d "build" ]; then
  rm -rf build
  mkdir build
else
  mkdir build 
fi

cd build
cmake ..
make
cd examples
time ./gpdense 

# export OPENCL_PATH=/usr/local/lib/mali/fbdev && export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$OPENCL_PATH
# g++ -o gaussian_elim gaussian_elim_host.cpp -I/usr/include -L$OPENCL_PATH -Wl,-rpath,$OPENCL_PATH -lOpenCL -std=c99 && ./gaussian_elim

# can check environment variables with the command "env"

# Search for (-l)OpenCL in directory specifed by the path (-L)$OPENCL_PATH.
# -Wl option .Pass option as an option to the linker. If option contains commas, it is split into multiple options at the commas.

# compiler takes issue with the -lOpenCL flag, even after environment variables have been set
