# Install script for directory: /home/s1350924/honproj/honproj

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/gp" TYPE FILE FILES
    "/home/s1350924/honproj/honproj/include/cov.h"
    "/home/s1350924/honproj/honproj/include/cov_factory.h"
    "/home/s1350924/honproj/honproj/include/cov_linear_ard.h"
    "/home/s1350924/honproj/honproj/include/cov_linear_one.h"
    "/home/s1350924/honproj/honproj/include/cov_matern3_iso.h"
    "/home/s1350924/honproj/honproj/include/cov_matern5_iso.h"
    "/home/s1350924/honproj/honproj/include/cov_noise.h"
    "/home/s1350924/honproj/honproj/include/cov_rq_iso.h"
    "/home/s1350924/honproj/honproj/include/cov_periodic_matern3_iso.h"
    "/home/s1350924/honproj/honproj/include/cov_periodic.h"
    "/home/s1350924/honproj/honproj/include/cov_se_ard.h"
    "/home/s1350924/honproj/honproj/include/cov_se_iso.h"
    "/home/s1350924/honproj/honproj/include/cov_sum.h"
    "/home/s1350924/honproj/honproj/include/cov_prod.h"
    "/home/s1350924/honproj/honproj/include/gp.h"
    "/home/s1350924/honproj/honproj/include/gp_utils.h"
    "/home/s1350924/honproj/honproj/include/sampleset.h"
    "/home/s1350924/honproj/honproj/include/rprop.h"
    "/home/s1350924/honproj/honproj/include/input_dim_filter.h"
    "/home/s1350924/honproj/honproj/include/cg.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/s1350924/honproj/honproj/build/libgp.a")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/s1350924/honproj/honproj/build/libgp.pc")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/s1350924/honproj/honproj/build/examples/cmake_install.cmake")
  INCLUDE("/home/s1350924/honproj/honproj/build/tests/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

IF(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
ELSE(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
ENDIF(CMAKE_INSTALL_COMPONENT)

FILE(WRITE "/home/s1350924/honproj/honproj/build/${CMAKE_INSTALL_MANIFEST}" "")
FOREACH(file ${CMAKE_INSTALL_MANIFEST_FILES})
  FILE(APPEND "/home/s1350924/honproj/honproj/build/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
ENDFOREACH(file)
