if("release-1.7.0" STREQUAL "")
  message(FATAL_ERROR "Tag for git checkout should not be empty.")
endif()

execute_process(
  COMMAND ${CMAKE_COMMAND} -E remove_directory "/home/s1350924/honproj/honproj/build/tests/gtest/src/googletest"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to remove directory: '/home/s1350924/honproj/honproj/build/tests/gtest/src/googletest'")
endif()

execute_process(
  COMMAND "/usr/bin/git" clone "https://github.com/google/googletest.git" "googletest"
  WORKING_DIRECTORY "/home/s1350924/honproj/honproj/build/tests/gtest/src"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to clone repository: 'https://github.com/google/googletest.git'")
endif()

execute_process(
  COMMAND "/usr/bin/git" checkout release-1.7.0
  WORKING_DIRECTORY "/home/s1350924/honproj/honproj/build/tests/gtest/src/googletest"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to checkout tag: 'release-1.7.0'")
endif()

execute_process(
  COMMAND "/usr/bin/git" submodule init
  WORKING_DIRECTORY "/home/s1350924/honproj/honproj/build/tests/gtest/src/googletest"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to init submodules in: '/home/s1350924/honproj/honproj/build/tests/gtest/src/googletest'")
endif()

execute_process(
  COMMAND "/usr/bin/git" submodule update --recursive
  WORKING_DIRECTORY "/home/s1350924/honproj/honproj/build/tests/gtest/src/googletest"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to update submodules in: '/home/s1350924/honproj/honproj/build/tests/gtest/src/googletest'")
endif()

