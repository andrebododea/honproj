/* kernel.cl 
 * Matrix multiplication: C = A * B.
 * Device code.
 */
 
// OpenCL Kernel

// Enables float precision doubleing point operations
// #pragma OPENCL EXTENSION cl_khr_fp64 : enable

__kernel void step_1_row_operation(__global float* A, __global float* k, __global float* diags, int wA )
{

    /* KERNEL #1 */
    // Do step 1 for each element of the current row: R_i = R_i/a_ii

   int index = get_global_id(0); 
   // http://stackoverflow.com/questions/9674179/getting-the-row-and-column-of-a-triangular-matrix-given-the-index


   if(index < wA*wA){ 

	   if(index < wA){
	     k[index] = k[index]/diags[index];
	   }

	     int row = index/wA;
	     int col = index%wA;
	     A[row*wA+col] = A[row*wA+col]/diags[col];
	   }
}


__kernel void step_2_col_operation(__global float* A, __global float* k, int wA, int currentRow)
{
   int col = get_global_id(0)+currentRow+1;
	  // Condition 1: as long as we haven't overrun the size of the matrix
	  if(col<wA ){ 
	    /** KERNEL #2 **/
	    k[col] = k[col]-A[currentRow*wA+col]*k[currentRow];
	  }
}


__kernel void step_2_col_operation_vec4(__global float* A, __global float* k, int wA, int currentRow)
{
   int i = get_global_id(0)+1;
   int vectorSize = 4;
   int numberOfVectorOps = (wA-currentRow+1)/vectorSize;
   int numberOfRegularOps = (wA-currentRow+1)%vectorSize;
   // All operations that are perfectly divisible by the vectorSize
   // These operations can use vector operations, all others need regular operations
   if(i < numberOfVectorOps+1){
           i = get_global_id(0)*4+currentRow+1;
          // Condition 1: as long as we haven't overrun the size of the matrix
           float mult = k[currentRow];
           float4 v_mult = (float4)(mult, mult, mult, mult);

            // Load solution vector and input matrix into float4 vector types
            int mat_offset = currentRow*wA+i;
            float4 matrix_value = vload4(0, A+mat_offset); 
            float4 vector_value = vload4(0, k+i);

            // Perform the multiplication and subtraction, then store
            //  back into the address: solutionVec + i * 4.            
            // k[col] = k[col]-A[currentRow*wA+col]*k[currentRow];
            vstore4(vector_value - matrix_value * v_mult, 0, k+i);
   }else if(i < numberOfVectorOps+numberOfRegularOps){
       int col = i+currentRow+4*numberOfVectorOps;
       k[col] = k[col]-A[currentRow*wA+col]*k[currentRow];
   }
} 


__kernel void step_2_col_operation_vec8(__global float* A, __global float* k, int wA, int currentRow)
{
   int i = get_global_id(0)+1;
   int vectorSize = 8;
   int numberOfVectorOps = (wA-currentRow+1)/vectorSize;
   int numberOfRegularOps = (wA-currentRow+1)%vectorSize;
   // All operations that are perfectly divisible by the vectorSize
   // These operations can use vector operations, all others need regular operations
   if(i < numberOfVectorOps+1){
           i = get_global_id(0)*vectorSize+currentRow+1;
          // Condition 1: as long as we haven't overrun the size of the matrix
           float mult = k[currentRow];
           // float8 v_mult = (float8)(mult, mult, mult, mult, mult, mult, mult, mult);

            // Load solution vector and input matrix into float4 vector types
            int mat_offset = currentRow*wA+i;
            float8 matrix_value = vload8(0, A+mat_offset);
            float8 vector_value = vload8(0, k+i);

            // Perform the multiplication and subtraction, then store
            //  back into the address: solutionVec + i * 4.
            // k[col] = k[col]-A[currentRow*wA+col]*k[currentRow];
            vstore8(vector_value - matrix_value * mult, 0, k+i);
   }else if(i < numberOfVectorOps+numberOfRegularOps){
       int col = i+currentRow+vectorSize*numberOfVectorOps;
       k[col] = k[col]-A[currentRow*wA+col]*k[currentRow];
   }
}


__kernel void step_2_col_operation_vec16(__global float* A, __global float* k, int wA, int currentRow)
{
   int i = get_global_id(0)+1;
   int vectorSize = 16;
   int numberOfVectorOps = (wA-currentRow+1)/vectorSize;
   int numberOfRegularOps = (wA-currentRow+1)%vectorSize;
   // All operations that are perfectly divisible by the vectorSize
   // These operations can use vector operations, all others need regular operations
   if(i < numberOfVectorOps+1){
           i = get_global_id(0)*16+currentRow+1;
          // Condition 1: as long as we haven't overrun the size of the matrix
           float mult = k[currentRow];
           float16 v_mult = (float16)(mult, mult, mult, mult, mult, mult, mult, mult, mult, mult, mult, mult, mult, mult, mult, mult);

            // Load solution vector and input matrix into float vector types
            int mat_offset = currentRow*wA+i;
            float16 matrix_value = vload16(0, A+mat_offset);
            float16 vector_value = vload16(0, k+i);

            // Perform the multiplication and subtraction, then store
            //  back into the address: solutionVec + i * 4.
            // k[col] = k[col]-A[currentRow*wA+col]*k[currentRow];
            vstore16(vector_value - matrix_value * mult, 0, k+i);
   }else if(i < numberOfVectorOps+numberOfRegularOps){
       int col = i+currentRow+16*numberOfVectorOps;
       k[col] = k[col]-A[currentRow*wA+col]*k[currentRow];
   }
}

    

/*
__kernel void step_3_zero_col(__global float* A, int wA, int currentRow)
{

   int row = get_global_id(0);

   int hA = wA-1;

  if(row != currentRow && row<hA){
    A[row*wA + currentRow] = 0;
  }
}
*/
