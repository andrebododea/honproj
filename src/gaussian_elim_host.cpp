///////////////////////////////////////////////////////////////////////////////
#include <malloc.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <CL/cl.h>
#include <stdbool.h>
#include <math.h>
#include <iostream>
#define _BSD_SOURCE
#include <sys/time.h>
#include <Eigen/Dense>
using namespace std;

// Header to allow call from gp.cc
// #include "opencl_gaussian_elimination.h" 

////////////////////////////////////////////////////////////////////////////////
// #define WA 101
// #define HA 100 
////////////////////////////////////////////////////////////////////////////////


// Allocates a matrix with random float entries.
void randomMemInit(double* k_in, double* L_in, cl_float* data, cl_float* vector_arr, cl_float* diag, Eigen::MatrixXd &eigen_matrix, Eigen::VectorXd &eigen_vector, int n, int size)
{
	for (int i = 0; i < size; ++i){
		   int row = i/n;
		   int col = i%n;
                   // takes the column major vector from eigen and copies it in col-major form
		   data[i] = (float)*(L_in+i);

                   if(row == col){
                        diag[row] = data[i];
                   }

                   eigen_matrix(col,row) = (double)*(L_in + i); 

                // Set random vector values
                if(col == n-1){
		    vector_arr[row] = *(k_in + row);
		    eigen_vector(row) = *(k_in + row);  
                }
       }
      
}

long LoadOpenCLKernel(char const* path, char **buf)
{
	FILE  *fp;
	size_t fsz;
	long   off_end;
	int    rc;

	/* Open the file */
	fp = fopen(path, "r");
	if( NULL == fp ) {
		return -1L;
	}

	/* Seek to the end of the file */
	rc = fseek(fp, 0L, SEEK_END);
	if( 0 != rc ) {
		return -1L;
	}

	/* Byte offset to the end of the file (size) */
	if( 0 > (off_end = ftell(fp)) ) {
		return -1L;
	}
	fsz = (size_t)off_end;

	/* Allocate a buffer to hold the whole file */
	*buf = (char *) malloc( fsz+1);
	if( NULL == *buf ) {
		return -1L;
	}

	/* Rewind file pointer to start of file */
	rewind(fp);

	/* Slurp file into buffer */
	if( fsz != fread(*buf, 1, fsz, fp) ) {
		free(*buf);
		return -1L;
	}

	/* Close the file */
	if( EOF == fclose(fp) ) {
		free(*buf);
		return -1L;
	}


	/* Make sure the buffer is NUL-terminated, just in case */
	(*buf)[fsz] = '\0';

	/* Return the file size */
	return (long)fsz;
}

//int run_gauss_elim()
//int run_gauss_elim(float* k_vector, double* L_matrix, int width)  
// int main( int argc, char *argv[] ) 
int triangParallel(double* k_in, double* L_in, int n_in)
{
        // Create eigen matrix
        Eigen::MatrixXd eigen_matrix(n_in,n_in);
        // Create eigen vector
        Eigen::VectorXd eigen_vector(n_in);
       // std::cout << "The matrix m is of size " << eigen_matrix.rows() << "x" << eigen_matrix.cols() << std::endl;


        // OpenCL Profiling Code (for timing)
        cl_event timing_event;
	cl_ulong time_queued, time_submit, time_start, time_end;
        unsigned long start_to_end = 0;
        unsigned long queue_to_submit = 0;
        unsigned long submit_to_start = 0;
        unsigned long total_time = 0;
        cl_ulong number_of_iterations = 30; // the number of times we want to run the event in order to get the average
        // Need to associate the cl_event, timing_event, with the command needing profiling. This is done by making the event pointer the last argument of the function that enqueues the command.


        // // TIMING CODE for entire program
	struct timeval tvalBefore, tvalAfter;


	int HA = n_in; 
	int WA = n_in;
	int input_local_work_size = 64;

	cout << "WA is of size: " << WA << "\n";
	cout << "local work size is of size: " << input_local_work_size << "\n";

	int err;                            // error code returned from api calls

	cl_device_id device_id;             // compute device id 
	cl_context context;                 // compute context
	cl_command_queue commands;          // compute command queue
	cl_program program;                 // compute program
	cl_kernel kernel1;                   // compute kernel
	cl_kernel kernel2;                   // compute kernel

	// OpenCL device memory for matrices
	cl_mem d_A;
        cl_mem d_vec_A;
        cl_mem diag_A;

	// set seed for rand()
 //	srand(2014);

	//Allocate host memory for matrices A and B
	int size_A = WA * HA;
	printf("size_A is %d", size_A);
	int mem_size_A = sizeof(float) * size_A;

        int result_vector_size = sizeof(float)*HA;


	// printf("Initializing OpenCL device...\n"); 


	cl_uint dev_cnt = 0;
	clGetPlatformIDs(0, 0, &dev_cnt);

	cl_platform_id platform_ids[100];
	clGetPlatformIDs(dev_cnt, platform_ids, NULL);

	// Connect to a compute device
	int gpu = 1;
	err = clGetDeviceIDs(platform_ids[0], CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to create a device group!\n");
		return EXIT_FAILURE;
	}

	// Create a compute context 
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	if (!context)
	{
		printf("Error: Failed to create a compute context!\n");
		return EXIT_FAILURE;
	}

    cl_uint integerVectorWidth;
    clGetDeviceInfo(device_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, sizeof(cl_uint), &integerVectorWidth, NULL);
    cout << "Prefered vector width for integers: " << integerVectorWidth << endl;


	// Create a command commands
	commands = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &err);  // profiling enabled with 3rd parameter
	if (!commands)
	{
		printf("Error: Failed to create a command commands!\n");
		return EXIT_FAILURE;
	}

	// Create the compute program from the source file
	char *KernelSource;
	long lFileSize;

	// lFileSize = LoadOpenCLKernel("/home/s1350924/honproj/honproj/src/gauss_methods_opencl/gauss_elim_parallel/gaussian_elim_kernel.cl", &KernelSource);
	lFileSize = LoadOpenCLKernel("/home/s1350924/honproj/honproj/src/gaussian_elim_kernel.cl", &KernelSource);
	if( lFileSize < 0L ) {
		perror("File read failed");
		return 1;
	}

	program = clCreateProgramWithSource(context, 1, (const char **) & KernelSource, NULL, &err);
	if (!program)
	{
		printf("Error: Failed to create compute program!\n");
		return EXIT_FAILURE;
	}

	// Build the program executable
	err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		size_t len;
		char buffer[2048];
		printf("Error: Failed to build program executable!\n");
		printf("%d", clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len));
                for(int i = 0 ; i < 2048 ; i ++ ){
                    cout << buffer[i] ;//Looping 5 times to print out [0],[1],[2],[3],[4]
                } 
		exit(1);
	}


	// Create the input and output arrays in device memory for our calculation
	d_A = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, mem_size_A, NULL, &err);
	if (!d_A)
	{
		printf("Error: Failed to allocate device memory!\n");
		exit(1);
	}    
        d_vec_A = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, result_vector_size, NULL, &err);
//      cl_float* res_A = (cl_float*)malloc(result_vector_size);
//      d_vec_A = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, result_vector_size, res_A, &err);  // Non mapped buffer
	if (!d_vec_A)
	{
		printf("Error: Failed to allocate device memory!\n");
		exit(1);
	}    

        diag_A = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, result_vector_size, NULL, &err);
	if (!d_vec_A)
	{
		printf("Error: Failed to allocate device memory!\n");
		exit(1);
	}    

	// http://stackoverflow.com/questions/17775738/cl-mem-alloc-host-ptr-slower-than-cl-mem-use-host-ptr
	cl_float* h_A = (cl_float*)clEnqueueMapBuffer(commands, d_A, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, mem_size_A, 0, NULL, NULL, &err); 
	cl_float* res_A = (cl_float*)clEnqueueMapBuffer(commands, d_vec_A, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, result_vector_size, 0, NULL, NULL, &err); 
	cl_float* diag_buffer = (cl_float*)clEnqueueMapBuffer(commands, diag_A, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, result_vector_size, 0, NULL, NULL, &err); 

	if(err){
		printf("You've got el error! %d", err);
	}



	//Initialize host memory
	randomMemInit(k_in, L_in, h_A, res_A, diag_buffer, eigen_matrix, eigen_vector, (int)WA, size_A); 


	// Unmap the buffer
	clEnqueueUnmapMemObject(commands, d_A, h_A, 0, NULL, NULL);
	clEnqueueUnmapMemObject(commands, d_vec_A, res_A, 0, NULL, NULL);
	clEnqueueUnmapMemObject(commands, diag_A, diag_buffer, 0, NULL, NULL);

	//Launch OpenCL kernel
	size_t localWorkSize[1], globalWorkSize[1];

	int wA = WA;

	// Set local and global work sizes for kernel 1
	localWorkSize[0] = input_local_work_size;
	int globSize = ((1+HA)*HA)/2+HA;   // The number of elements in the triangular matrix, + HA (the number of elements in the result vector k)
	int shader_cores = 4;
	int multiplicand = shader_cores * input_local_work_size; // Multiply by a constant??? 4 or 8
	if(globSize%multiplicand != 0){
		globSize = globSize-(globSize%multiplicand)+multiplicand; // Round up globSize to the nearest number that divides by local work size
	}
	globalWorkSize[0] = globSize;

	cout << "Global work size for kernel 1 is of size: " << globSize << "\n";





	// BEGIN GAUSSIAN ELIMINATION

	// Create the compute kernel in the program we wish to run
	kernel1 = clCreateKernel(program, "step_1_row_operation", &err);
	if (!kernel1 || err != CL_SUCCESS)
	{
		printf("Error: Failed to create compute kernel!\n");
		exit(1);
	}

//	 kernel2 = clCreateKernel(program, "step_2_col_operation", &err);
	// kernel2 = clCreateKernel(program, "step_2_col_operation_vec4", &err);
	kernel2 = clCreateKernel(program, "step_2_col_operation_vec8", &err);
	if (!kernel2 || err != CL_SUCCESS)
	{
		printf("Error: Failed to create compute kernel!\n");
		exit(1);
	}
  


	/** QUERY LOCAL MEMORY SIZE **/
	cl_int local_mem_size;
	clGetDeviceInfo(device_id, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(local_mem_size), &local_mem_size, NULL);
	// printf("Local memory size is: %d \n", local_mem_size);


	/** QUERY GLOBAL MEMORY SIZE **/
	cl_int global_mem_size;
	clGetDeviceInfo(device_id, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(global_mem_size), &global_mem_size, NULL);
	// printf("Global memory size is: %d \n", global_mem_size);


        // Querying memory alignment
	cl_int alignment_mem_size;
	clGetDeviceInfo(device_id, CL_DEVICE_MEM_BASE_ADDR_ALIGN, sizeof(alignment_mem_size), &alignment_mem_size, NULL);
	// printf("Alignment size is: %d \n", alignment_mem_size);

	/** QUERY WORKGROUP SIZE FOR KERNEL 1 **/
	size_t workgroup_size;
	err = clGetKernelWorkGroupInfo(kernel1, device_id, CL_KERNEL_WORK_GROUP_SIZE,
			sizeof(size_t), &workgroup_size, NULL);
	if (err != CL_SUCCESS) { printf("Unable to get kernel1 work-group size"); }
	// printf("Kernel1 work group size is: %d\n", workgroup_size);


	/** QUERY WORKGROUP SIZE FOR KERNEL 2 **/
	err = clGetKernelWorkGroupInfo(kernel2, device_id, CL_KERNEL_WORK_GROUP_SIZE,
			sizeof(size_t), &workgroup_size, NULL);
	if (err != CL_SUCCESS) { printf("Unable to get kernel2 work-group size"); }
	printf("Kernel2 work group size is: %d\n", workgroup_size);


	/*
	 * Query the device to find out it's prefered float vector width.
	 * Although we are only printing the value here, it can be used to select between
	 * different versions of a kernel.
	 */
	/* 
	   cl_float doubleVectorWidth;
	   clGetDeviceInfo(device_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, sizeof(cl_float), &doubleVectorWidth, NULL);
	   printf("Prefered vector width for floats: %d", doubleVectorWidth);
	 */
/*
           printf("\n\nMatrix A (Starting matrix)\n");
           for(unsigned long i = 0; i < size_A; i++)
           {
           printf("%f ", h_A[i]);

        // Add in new lines
        if(((i+1) % WA) == 0){
           printf("%f  !", res_A[i/WA]);
	   printf("\n");
        }
        }
        printf("\n");
        printf("\n");

      printf("Eigen matrix");
      for(int i = 0; i < HA; i++){
         for(int j = 0; j < HA; j++){
 
              printf("%f ",eigen_matrix(i,j));
          }
          printf("\n");
      }

*/

                
	 struct timespec ts_start_k1;
        struct timespec ts_end_k1;
        clock_gettime(CLOCK_MONOTONIC, &ts_start_k1);

		/************/
		/* Kernel 1 */
		/************/
		// Make diagonal component == 1 by dividing all non-zero elements by it.

		// Setting arguments for the kernel
		err = clSetKernelArg(kernel1, 0, sizeof(cl_mem), (void *)&d_A);
                err |= clSetKernelArg(kernel1, 1, sizeof(cl_mem), (void *)&d_vec_A); 
                err |= clSetKernelArg(kernel1, 2, sizeof(cl_mem), (void *) &diag_A);
		err |= clSetKernelArg(kernel1, 3,sizeof(cl_int), (void *)&wA);

		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to set kernel arguments for kernel 1! %d\n", err);
			exit(1);
		}
                
		

		// This is where execution of the kernel is actually done
		err = clEnqueueNDRangeKernel(commands, kernel1, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);

		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to execute kernel1! %d\n", err);
			exit(1);
		}
                
                

		h_A = (cl_float*)clEnqueueMapBuffer(commands, d_A, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, mem_size_A, 0, NULL, NULL, &err);
                res_A = (cl_float*)clEnqueueMapBuffer(commands, d_vec_A, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, result_vector_size, 0, NULL, NULL, &err); 
                diag_buffer = (cl_float*)clEnqueueMapBuffer(commands, diag_A, CL_TRUE, CL_MAP_READ, 0, result_vector_size, 0, NULL, NULL, &err);


               //Retrieve result from device
               /*               
                err = clEnqueueReadBuffer(commands, d_vec_A, CL_TRUE, 0, result_vector_size, res_A, 0, NULL, NULL);
	       if (err != CL_SUCCESS)
	       {
		   printf("Error: Failed to read output array! %d\n", err);
		   exit(1);
	       }
               */


		//Retrieve result from device
		clEnqueueUnmapMemObject(commands, d_A, h_A, 0, NULL, NULL);       
		clEnqueueUnmapMemObject(commands, d_vec_A, res_A, 0, NULL, NULL);
	        clEnqueueUnmapMemObject(commands, diag_A, diag_buffer, 0, NULL, NULL);

    // TIMING CODE FOR Kernel 1 
        clock_gettime(CLOCK_MONOTONIC, &ts_end_k1);
        long kernel1time = ((ts_end_k1.tv_sec - ts_start_k1.tv_sec)*1000000L+ts_end_k1.tv_nsec/1000) - ts_start_k1.tv_nsec/1000; 
        printf("Monotonic time for kernel 1 loop is %ld microseconds \n", kernel1time);

/*
           printf("\n\nMatrix A (Starting matrix)\n");
           for(int i = 0; i < size_A; i++)
           {
           printf("%f ", h_A[i]);

        // Add in new lines
        if(((i+1) % WA) == 0){
           printf("%f  !", res_A[i/WA]);
           printf("\n");
        }
        }
        printf("\n");
        printf("\n");
*/
              
		/************/
		/* Kernel 2 */
		/************/
	//Launch OpenCL kernel
        gettimeofday (&tvalBefore, NULL);


	// Main timing segment
	 struct timespec ts_start_k2loop;
        struct timespec ts_end_k2loop;

        long total_k2_monotonic = 0;

      clock_gettime(CLOCK_MONOTONIC, &ts_start_k2loop);
      for(int currentRow = 0; currentRow < WA; currentRow++){

		// Set local and global work sizes for kernel 2 
		size_t localWorkSize2[1], globalWorkSize2[1];
		globSize = HA-currentRow;

                // Next two lines are vectorisation lines
                int vectorSize = 8;
		globSize = globSize/vectorSize+globSize%vectorSize;

		if(globSize%multiplicand != 0){
			// Non-vectorised globSize
                        //printf("Performing operation on row %d with real size of %d, global size of %d, and rounded size of %d \n",currentRow,HA-currentRow,globSize,globSize-(globSize%multiplicand)+multiplicand);
			globSize = globSize-(globSize%multiplicand)+multiplicand; // Round up globSize to the nearest number that divides by local work size
		}else{

                        //printf("Performing operation on row %d with real size of %d and no rounding necessary \n",currentRow,globSize);
		}
        
		localWorkSize2[0] = input_local_work_size;
		globalWorkSize2[0] = globSize;

		// cout << "Global work size for kernel 2 is of size: " << globSize << "\n";

		err = clSetKernelArg(kernel2, 0, sizeof(cl_mem), (void *)&d_A);
                err |= clSetKernelArg(kernel2, 1, sizeof(cl_mem), (void *)&d_vec_A); 
		err |= clSetKernelArg(kernel2, 2, sizeof(int), (void *)&wA);
		err |= clSetKernelArg(kernel2, 3, sizeof(int), (void *)&currentRow);

		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to set kernel arguments! %d\n", err);
			exit(1);
		}

		//print out the results
/*
		  printf("\n\nMatrix A (before kernel) \n");
		   for(int i = currentCol; i < wA; i++)
		   {
		   printf("k: %f - %f ",res_A[i], h_A[i*wA+currentCol]);

		// Add in new lines
		if(((i + 1) % WA) == 0){
		printf("\n");
		}
		}
		printf("\n");
*/

		// This is where execution of the kernel is actually done

        
                res_A =  (cl_float*)clEnqueueMapBuffer(commands, d_vec_A, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, result_vector_size, 0, NULL, NULL, &err); 

	        err = clEnqueueNDRangeKernel(commands, kernel2, 1, NULL, globalWorkSize2, localWorkSize2, 0, NULL, NULL);
                // clFinish(commands);
		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to execute kernel2! %d\n", err);
			exit(1);
		}

/*
	       clGetEventProfilingInfo(timing_event, CL_PROFILING_COMMAND_QUEUED, sizeof(time_queued), &time_queued, NULL);
	       clGetEventProfilingInfo(timing_event, CL_PROFILING_COMMAND_SUBMIT, sizeof(time_submit), &time_submit, NULL);
	       clGetEventProfilingInfo(timing_event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
	       clGetEventProfilingInfo(timing_event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
               queue_to_submit += (unsigned long)time_submit - (unsigned long)time_queued;
               submit_to_start += time_start - time_submit;
	       start_to_end += time_end - time_start;
               total_time += time_end - time_queued;
*/
		clEnqueueUnmapMemObject(commands, d_vec_A, res_A, 0, NULL, NULL);
/*
			   printf("\n\nMatrix A (Starting matrix)\n");
			   for(int i = 0; i < size_A; i++)
			   {
			   printf("%f ", h_A[i]);

			// Add in new lines
			if(((i+1) % WA) == 0){
			   printf("%f  ! res_A[%d]", res_A[i/WA], i/WA);
			   printf("\n");
			}
			}
			printf("\n");
			printf("\n");
*/
                // CL_PROFILING_COMMAND_START

               //Retrieve result from device

	 }

	clock_gettime(CLOCK_MONOTONIC, &ts_end_k2loop);
	total_k2_monotonic = ((ts_end_k2loop.tv_sec - ts_start_k2loop.tv_sec)*1000000+ts_end_k2loop.tv_nsec/1000) - ts_start_k2loop.tv_nsec/1000; 
        printf("Monotonic time for kernel 2 is %lu microseconds \n", total_k2_monotonic);

        // At the end of all looping
/*
        printf("OPENCL TIMING: For n = %d, total time spent UNMAPPING is:\n", n_in);
        printf("Time from queue to submit is %lu microseconds\n", queue_to_submit/1000);
        printf("Time from submit to start is %lu microseconds\n", submit_to_start/1000);
	printf("Time from start to end    is %lu microseconds\n", start_to_end/1000);
        printf("Total time                is %lu microseconds\n", total_time/1000);

       printf("%d, %lu, %lu, %lu, %lu \n",n_in, queue_to_submit/1000, submit_to_start/1000, start_to_end/1000, total_time/1000);
        
        long kernel2time = ((ts_end_k2loop.tv_sec - ts_start_k2loop.tv_sec)*1000000+ts_end_k2loop.tv_nsec/1000) - ts_start_k2loop.tv_nsec/1000; 
        printf("Monotonic time for THE WHOLE PROGRAM kernel 1 and kernel 2 loop is end:%lu:%lu - start:%lu:%lu =  %lu microseconds \n", ts_end_k2loop.tv_sec, ts_end_k2loop.tv_nsec, ts_start_k2loop.tv_sec, ts_start_k2loop.tv_nsec, kernel2time);
*/
        long totalTime = ((ts_end_k2loop.tv_sec - ts_start_k1.tv_sec)*1000000L+ts_end_k2loop.tv_nsec/1000) - ts_start_k1.tv_nsec/1000; 
  //      printf("Monotonic time for whole program without setup is %ld microseconds \n", totalTime);

      // Timing code for eigen matrix
	 struct timespec ts_eigen_start;
        struct timespec ts_eigen_end;
        clock_gettime(CLOCK_MONOTONIC, &ts_eigen_start);
       // Do Eigen linear equation solving
      // eigen_matrix.topLeftCorner(HA,HA).triangularView<Eigen::Lower>().solveInPlace(eigen_vector);
       eigen_matrix.triangularView<Eigen::Lower>().solveInPlace(eigen_vector);
        clock_gettime(CLOCK_MONOTONIC, &ts_eigen_end);
    long el_t2 =  ((ts_eigen_end.tv_sec - ts_eigen_start.tv_sec)*1000000+ts_eigen_end.tv_nsec/1000) - ts_eigen_start.tv_nsec/1000;
    printf("EIGEN -- For size %d, TOTAL time in microseconds: %ld microseconds\n",n_in,el_t2);


/*
      printf("Eigen matrix");
      for(int i = 0; i < HA; i++){
         for(int j = 0; j < HA; j++){

              printf("%f ",eigen_matrix(i,j));
          }
          printf("\n");
      }
*/




      /*
       printf("Solution vector of OpenCL: \n");
       for(int i = 0; i < HA; i++){
	  printf("%f, ", k_in[i]);
       } 

       double mean_difference = 0; 
       for(int i = 0; i < HA; i++){
          float eig_val = eigen_vector(i);
          // printf("%f, ", eig_val);
          float opencl_val = res_A[i];
          mean_difference += fabs(fabs(eig_val) - fabs(opencl_val));
       } 
       mean_difference = mean_difference/HA;
       printf("\n");

       printf("Opencl diff is: %f \n", mean_difference);

*/




	//Shutdown and cleanup

	// Unmap the buffer after all is said and done
	clEnqueueUnmapMemObject(commands, d_A, h_A, 0, NULL, NULL);

	clReleaseMemObject(d_A);
	clReleaseMemObject(d_vec_A);
	clReleaseMemObject(diag_A);

	clReleaseProgram(program);
	clReleaseKernel(kernel1);
	clReleaseKernel(kernel2);
	clReleaseCommandQueue(commands);
	clReleaseContext(context);



	return 0;
}


