#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <CL/cl.h>
#include <stdbool.h>
#include <math.h>
#include <iostream>
#define _BSD_SOURCE
#include <sys/time.h>
#include <Eigen/Dense>
// using namespace Eigen;
using namespace std;


int triangSerial(double* k_in, double* L_in, int n){

        // int n = 600;

        float* matrix_arr = (float*)malloc(sizeof(float)*n*n);
        float* vector_arr = (float*)malloc(sizeof(float)*n);
        double* diags = (double*)malloc(sizeof(double)*n);
        
        Eigen::MatrixXd eigen_matrix(n,n);
        Eigen::VectorXd eigen_vector(n);

        int fMin = 0;
        int fMax = 1.5;

       // Copy the matrix and diagonal arrays
        for (int i = 0; i < n*n; ++i){
                   int row = i/n;
                   int col = i%n;
                   matrix_arr[col*n+row] = (float)*(L_in+i);

                   if(row == col){
                        diags[row] = matrix_arr[i];
                   }

                   eigen_matrix(col,row) = (double)*(L_in + i);
       }

       // Copy the vector arrays
       for(int i = 0; i < n; i++){
            vector_arr[i] = *(k_in + i); 
            eigen_vector(i) = *(k_in + i);
       }
/*
       printf("\n\nMatrix A (Starting matrix)\n");
           for(unsigned long i = 0; i < n*n; i++)
           {
           printf("%f ", L_in[i]);

        // Add in new lines
        if(((i+1) % n) == 0){
           printf("\n");
        }
        }
        printf("\n");
        printf("\n");
*/
 
 // TIMING CODE

struct timeval tvalBefore, tvalAfter;
gettimeofday (&tvalBefore, NULL);

       // Noramlise matrix. (kernel 1)
       for(int i = 0; i < n*(n); i++){
	     int row = i/n;
	     int col = i%n;
//	     matrix_arr[row*n+col] = matrix_arr[row*n+col]/diags[row];
             L_in[row*n+col] = L_in[row*n+col]/diags[col];
       }
       // Do division on vector (also kernel 1)
       for(int i = 0; i < n; i++){
	       k_in[i] = k_in[i]/diags[i];
       }

       /*
       printf("\n\nMatrix A (Starting normalised)\n");
           for(unsigned long i = 0; i < n*n; i++)
           {
           printf("%f ", L_in[i]);

        // Add in new lines
        if(((i+1) % n) == 0){
           printf("%f  !", k_in[i/n]);
           printf("\n");
        }
        }
        printf("\n");
        printf("\n");
        */

       // Perform elimination. (kernel 2)
       for(int currentRow = 0; currentRow < n; currentRow++){
	       for(int col = currentRow+1; col < n; col++){
		     k_in[col] = k_in[col]-L_in[currentRow*n+col]*k_in[currentRow];
//		     printf("row,col= (%d,%d)... %f = %f - %f * %f \n", currentRow, col, k_in[col], k_in[col], L_in[currentRow*n+col], k_in[currentRow]);
	       }
       }

       // TIMING CODE:
     gettimeofday (&tvalAfter, NULL);

    printf("For size %d, time in microseconds: %ld microseconds\n",n,
            ((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L
           +tvalAfter.tv_usec) - tvalBefore.tv_usec
          ); // Added semicolon
       



       // Perform elimination on the Eigen matrix
gettimeofday (&tvalBefore, NULL);
       eigen_matrix.triangularView<Eigen::Lower>().solveInPlace(eigen_vector);
     gettimeofday (&tvalAfter, NULL);

    printf("EIGEN: For size %d, time in microseconds: %ld microseconds\n",n,
            ((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L
           +tvalAfter.tv_usec) - tvalBefore.tv_usec
          ); // Added semicolon


       // Print vector solutions
       // printf("Final vector array solution \n");
/*
       for(int i = 0; i < n; i++){
           printf("%f, ", k_in[i]);
          }
	
       printf("\n \n");
       printf("Final eigen vector array solution \n");
       for(int i = 0; i < n; i++){
          double eig_val = eigen_vector(i);
          printf("%f, ", eig_val);
          }
       printf("\n \n");
  */     

        // calculate difference between my implementation and eigen's
       double mean_difference = 0; 
       for(int i = 0; i < n; i++){
          double eig_val = eigen_vector(i);
          double opencl_val = k_in[i];
          mean_difference += fabs(fabs(eig_val) - fabs(opencl_val));
         // printf("Mean difference = %f:: ",mean_difference); 
       } 
       mean_difference = mean_difference/n;
        printf("Mean difference between eig and serial triangular solve is: %f \n", mean_difference);

        free(matrix_arr);
        free(vector_arr);
        free(diags);

}

