// libgp - Gaussian process library for Machine Learning
// Copyright (c) 2013, Manuel Blum <mblum@informatik.uni-freiburg.de>
// All rights reserved.

#include "gp.h"
#include "cov_factory.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <ctime>

// OpenCL program header
#include "opencl_gaussian_elimination.h" 


using namespace std;

namespace libgp {
  
  const double log2pi = log(2*M_PI);
  const double initial_L_size = 1000;

  GaussianProcess::GaussianProcess ()
  {
      sampleset = NULL;
      cf = NULL;
  }

  GaussianProcess::GaussianProcess (size_t input_dim, std::string covf_def)
  {
    timingVar = 0;  // TIMING CODE
    printTime = true;
    timingVar2 = 0; // TIMING CODE
    // set input dimensionality
    this->input_dim = input_dim;
    // create covariance function
    CovFactory factory;
    cf = factory.create(input_dim, covf_def);
    cf->loghyper_changed = 0;
    sampleset = new SampleSet(input_dim);
    L.resize(initial_L_size, initial_L_size);
  }
  
  GaussianProcess::GaussianProcess (const char * filename) 
  {
    sampleset = NULL;
    cf = NULL;
    int stage = 0;
    std::ifstream infile;
    double y;
    infile.open(filename);
    std::string s;
    double * x = NULL;
    L.resize(initial_L_size, initial_L_size);
    while (infile.good()) {
      getline(infile, s);
      // ignore empty lines and comments
      if (s.length() != 0 && s.at(0) != '#') {
        std::stringstream ss(s);
        if (stage > 2) {
          ss >> y;
          for(size_t j = 0; j < input_dim; ++j) {
            ss >> x[j];
          }
          add_pattern(x, y);
        } else if (stage == 0) {
          ss >> input_dim;
          sampleset = new SampleSet(input_dim);
          x = new double[input_dim];
        } else if (stage == 1) {
          CovFactory factory;
          cf = factory.create(input_dim, s);
          cf->loghyper_changed = 0;
        } else if (stage == 2) {
          Eigen::VectorXd params(cf->get_param_dim());
          for (size_t j = 0; j<cf->get_param_dim(); ++j) {
            ss >> params[j];
          }
          cf->set_loghyper(params);
        }
        stage++;
      }
    }
    infile.close();
    if (stage < 3) {
      std::cerr << "fatal error while reading " << filename << std::endl;
      exit(EXIT_FAILURE);
    }
    delete [] x;
  }
  
  GaussianProcess::GaussianProcess(const GaussianProcess& gp)
  {
    this->input_dim = gp.input_dim;
    sampleset = new SampleSet(*(gp.sampleset));
    alpha = gp.alpha;
    k_star = gp.k_star;
    alpha_needs_update = gp.alpha_needs_update;
    L = gp.L;
    
    // copy covariance function
    CovFactory factory;
    cf = factory.create(gp.input_dim, gp.cf->to_string());
    cf->loghyper_changed = gp.cf->loghyper_changed;
    cf->set_loghyper(gp.cf->get_loghyper());
  }
  
  GaussianProcess::~GaussianProcess()
  {
    // free memory
    if (sampleset != NULL) delete sampleset;
    if (cf != NULL) delete cf;
  }  
  
  // Predict target value for given input. 
  // Returns predicted value
  double GaussianProcess::f(const double x[])
  {
    if(printTime){
	    printf("Time spent executing the thing is: %f", timingVar);
            printTime = false;
    }
    if (sampleset->empty()) return 0;
    Eigen::Map<const Eigen::VectorXd> x_star(x, input_dim);
    compute(); // Note to self: does nothing 
    update_alpha(); 
    update_k_star(x_star);
    return k_star.dot(alpha);    // Gives the target value based on the f(x) calculated earlier. Returns k_star.dot(alpha) = f* = Gaussian process posterior mean ??
  }
  
  double GaussianProcess::var(const double x[])
  {
    if (sampleset->empty()) return 0;
    Eigen::Map<const Eigen::VectorXd> x_star(x, input_dim);
    compute();
    update_alpha();
    update_k_star(x_star);
    int n = sampleset->size();
    Eigen::VectorXd v = L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solve(k_star);
    return cf->get(x_star, x_star) - v.dot(v);	
  }

  // Compute covariance matrix and perform cholesky decomposition. 
  void GaussianProcess::compute()
  {

    // can previously computed values be used?
    // Note to self: never get past here
    if (!cf->loghyper_changed) return;

    cf->loghyper_changed = false;
    int n = sampleset->size();
    // resize L if necessary
    if (n > L.rows()) L.resize(n + initial_L_size, n + initial_L_size);
    // compute kernel matrix (lower triangle)
    for(size_t i = 0; i < sampleset->size(); ++i) {
      for(size_t j = 0; j <= i; ++j) {
        std::cout << "L(i,j) before: " << L(i, j) << std::endl;
        L(i, j) = cf->get(sampleset->x(i), sampleset->x(j));
        std::cout << "L(i,j) before: " << L(i, j) << std::endl;
      }
    }
    // perform cholesky factorization
    //solver.compute(K.selfadjointView<Eigen::Lower>());
    L.topLeftCorner(n, n) = L.topLeftCorner(n, n).selfadjointView<Eigen::Lower>().llt().matrixL();
    alpha_needs_update = true;
  }
  
  // Update test input and cache kernel vector. 
  void GaussianProcess::update_k_star(const Eigen::VectorXd &x_star)
  {
    k_star.resize(sampleset->size());
    for(size_t i = 0; i < sampleset->size(); ++i) {
      k_star(i) = cf->get(x_star, sampleset->x(i));
    }
  }

  void GaussianProcess::update_alpha()
  {
    // can previously computed values be used?
    if (!alpha_needs_update) return;
    alpha_needs_update = false;
    alpha.resize(sampleset->size());
    // Map target values to VectorXd
    const std::vector<double>& targets = sampleset->y();
    Eigen::Map<const Eigen::VectorXd> y(&targets[0], sampleset->size());
    int n = sampleset->size();
    alpha = L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solve(y);   // this is alpha = L \ y, where L \ y is the vector x which solves Lx = y. Otherwise put, alpha = y * L^-1 or alpha = y * inv(L). See Line 3 on page 19 of Rasmussen
    L.topLeftCorner(n, n).triangularView<Eigen::Lower>().adjoint().solveInPlace(alpha); // this is alpha = transpose(L)\alpha . Otherwise put: alpha = alpha * inv(transpose(L))
  }


// FOR TIME WASTER, PROFILING CODE....CAN DELETE:
/*
  Eigen::VectorXd GaussianProcess::time_waster(Eigen::MatrixXd L, Eigen::VectorXd k, int n)
{
      L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solveInPlace(k);
      return k;
}
*/


    // Function timing code
    /*
    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::chrono::duration<double> elapsed_seconds;
    start = std::chrono::system_clock::now();
    FUNCTION CALL ()
    end = std::chrono::system_clock::now();
    elapsed_seconds += end-start;
    std::cout << "elapsed time: " << elapsed_seconds.count() << "s\n";
*/
  
  void GaussianProcess::add_pattern(const double x[], double y)
  {
    //std::cout<< L.rows() << std::endl;
#if 0
    sampleset->add(x, y);
    cf->loghyper_changed = true;
    alpha_needs_update = true;
    cached_x_star = NULL;
    return;
#else
    int n = sampleset->size();
    sampleset->add(x, y);
    // create kernel matrix if sampleset is empty
    if (n == 0) {
        // cf is the covariance function
      L(0,0) = sqrt(cf->get(sampleset->x(0), sampleset->x(0)));
      cf->loghyper_changed = false;
      std::cout << "First " << std::endl;

      // recompute kernel matrix if necessary
      // (note to self: kernel matrix == covariance matrix)
    } else if (cf->loghyper_changed) {
      compute();

     //
    // update kernel matrix (vast majority of iterations of input samples n, are spent updating the matrix)
    } else {
      Eigen::VectorXd k(n);

     clock_t bang = clock(); // TIMING CODE
        for (int i = 0; i<n; ++i) {
        //  std::cout << "i = " << i << "....  n = " << n << std::endl;
        k(i) = cf->get(sampleset->x(i), sampleset->x(n));
      }
     
        double kappa = cf->get(sampleset->x(n), sampleset->x(n));

         
      // resize L if necessary (resizing is rare, once every 1000 n's)
      if (sampleset->size() > static_cast<std::size_t>(L.rows())) {
          std::cout << "Resizing " << std::endl;
        L.conservativeResize(n + initial_L_size, n + initial_L_size);
      }


      /*************************************************************/
      /*******   call to OpenCL linear equation solver       *******/
      /*************************************************************/

      double *k_pass_out;                // NULL pointer, double because k is full of doubles
     // Eigen::Map<Eigen::MatrixXd>(k_pass_out, 1, n ) = k; // map the Eigen::VectorKd k(n) to k_pass_out

     if(n==10 || n==32 || n==64 || n==96 || n==128 || n==512 || n==1024 || n==2048 || n==3072  || n==4096 || n==5120 || n==6144 || n==7168 || n==8192 || n==9500) {
	       // std::cout << "Actual matrix is: " << L.topLeftCorner(n,n) << std::endl << std::endl; 
              clock_t start = clock(); // TIMING CODE
	       Eigen::MatrixXd L_out = L.topLeftCorner(n,n);
               Eigen::VectorXd k_out = k;
              // std::cout << "For operation " << n << "Time to copy the top left corner: " << (double(clock() - start) / CLOCKS_PER_SEC) << std::endl; // TIMING CODE
//	       triangSerial(k_out.data(),L_out.data(),n);
               triangParallel(k_out.data(),L_out.data(),n);
       }




      /***********************************/
      /*******  End implementation *******/
      /***********************************/



      clock_t start = clock(); // TIMING CODE
      // PARALLELIZATION DONE HERE
        // L.topLeftCorener(n,n) takes an n x n block of the top left hand corner of L.
     
     // std::cout << "Here is the matrix L:\n" << L.topLeftCorner(n,n) << "\n\n\n\n" << std::endl; 
    
     L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solveInPlace(k);    // Linear equation: L*k = k or k = k * inv(L)

      


      /*
      printf("k, after being solved for is: \n");
   for(int i = 0; i < n; i++){
          printf("%f, ", k(i));
       }
      printf("\n");
     */


        timingVar +=(double(clock() - start) / CLOCKS_PER_SEC); // TIMING CODE
        // cout << "Time spent executing: " <<  timingVar << endl; // TIMING CODE

      // Taking from L the 1 x n (rows x cols) sub-matrix starting from position (n,0)
      // Then setting that equal to k.transpose()
     // Effectively, you're setting the n+1 row equal to the vector k, then setting the n+1xn+1 element to sqrt(kappa - k.dot(k))
      L.block(n,0,1,n) = k.transpose();
      
      // coefficient (n,n) of L is set equal to sqrt(kappa - k.dot(k))
       L(n,n) = sqrt(kappa - k.dot(k));
   
	if(n<=10){
       std::cout << "Iteration :" << n << std::endl; 
       std::cout << L.topLeftCorner(n,n) << std::endl << std::endl; 
	}
    }
    alpha_needs_update = true;
    // printf("\n \n");

#endif
  }

  bool GaussianProcess::set_y(size_t i, double y) 
  {
    if(sampleset->set_y(i,y)) {
      alpha_needs_update = true;
      return 1;
    }
    return false;
  }

  size_t GaussianProcess::get_sampleset_size()
  {
    return sampleset->size();
  }
  
  void GaussianProcess::clear_sampleset()
  {
    sampleset->clear();
  }
  
  void GaussianProcess::write(const char * filename)
  {
    // output
    std::ofstream outfile;
    outfile.open(filename);
    time_t curtime = time(0);
    tm now=*localtime(&curtime);
    char dest[BUFSIZ]= {0};
    strftime(dest, sizeof(dest)-1, "%c", &now);
    outfile << "# " << dest << std::endl << std::endl
    << "# input dimensionality" << std::endl << input_dim << std::endl 
    << std::endl << "# covariance function" << std::endl 
    << cf->to_string() << std::endl << std::endl
    << "# log-hyperparameter" << std::endl;
    Eigen::VectorXd param = cf->get_loghyper();
    for (size_t i = 0; i< cf->get_param_dim(); i++) {
      outfile << std::setprecision(10) << param(i) << " ";
    }
    outfile << std::endl << std::endl 
    << "# data (target value in first column)" << std::endl;
    for (size_t i=0; i<sampleset->size(); ++i) {
      outfile << std::setprecision(10) << sampleset->y(i) << " ";
      for(size_t j = 0; j < input_dim; ++j) {
        outfile << std::setprecision(10) << sampleset->x(i)(j) << " ";
      }
      outfile << std::endl;
    }
    outfile.close();
  }
  
  CovarianceFunction & GaussianProcess::covf()
  {
    return *cf;
  }
  
  size_t GaussianProcess::get_input_dim()
  {
    return input_dim;
  }

  double GaussianProcess::log_likelihood()
  {
    compute();
    update_alpha();
    int n = sampleset->size();
    const std::vector<double>& targets = sampleset->y();
    Eigen::Map<const Eigen::VectorXd> y(&targets[0], sampleset->size());
    double det = 2 * L.diagonal().head(n).array().log().sum();
    return -0.5*y.dot(alpha) - 0.5*det - 0.5*n*log2pi;
  }

  Eigen::VectorXd GaussianProcess::log_likelihood_gradient() 
  {
    compute();
    update_alpha();
    size_t n = sampleset->size();
    Eigen::VectorXd grad = Eigen::VectorXd::Zero(cf->get_param_dim());
    Eigen::VectorXd g(grad.size());
    Eigen::MatrixXd W = Eigen::MatrixXd::Identity(n, n);

    // compute kernel matrix inverse
    L.topLeftCorner(n, n).triangularView<Eigen::Lower>().solveInPlace(W);
    L.topLeftCorner(n, n).triangularView<Eigen::Lower>().transpose().solveInPlace(W);

    W = alpha * alpha.transpose() - W;

    for(size_t i = 0; i < n; ++i) {
      for(size_t j = 0; j <= i; ++j) {
        cf->grad(sampleset->x(i), sampleset->x(j), g);
        if (i==j) grad += W(i,j) * g * 0.5;
        else      grad += W(i,j) * g;
      }
    }

    return grad;
  }
}
