///////////////////////////////////////////////////////////////////////////////

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <CL/cl.h>
#include <stdbool.h>
#include <math.h>
#include <iostream>
#include <Eigen>
using namespace Eigen;
using namespace std;

// Header to allow call from gp.cc
// #include "opencl_gaussian_elimination.h" 

////////////////////////////////////////////////////////////////////////////////
// #define WA 101
// #define HA 100 
////////////////////////////////////////////////////////////////////////////////


// Allocates a matrix with random float entries.
void randomMemInit(cl_float* data, cl_float* vector_arr, cl_float* diag, Eigen::MatrixXd &matrix, Eigen::VectorXd &vector, int wEig, unsigned long size)
{

        int fMin = 0;
        int fMax = 1.5;

	int needed_width = 0;     
	for (int i = 0; i < size; ++i){
		   int row = i/wEig;
		   int col = i%wEig;
	           cl_float f = rand() / (float)RAND_MAX;
	        if(col <= needed_width){ 
			// random float uniformly distributed between 0 and 1
			data[i] = fMin + f * (fMax - fMin);
                }else{
			data[i] = 0;
                }
                
                // Set diagonal matrix values
		if(row == col){
		    diag[row] = fMin + f * (fMax - fMin);
               }

                // Set random vector values
                if(col == wEig-1){
                   needed_width++; 
                   cl_float ra = rand() / (float)RAND_MAX;
                   vector_arr[row] = fMin + ra * (fMax - fMin); 
                   printf(" %f \n ", vector_arr[row]);
                   vector(row) = fMin + ra * (fMax - fMin); 
                   
                }

               // Assign values for the Eigen matrix
               matrix(row,col) = fMin + f * (fMax - fMin);
	       printf("matrix(%d,%d) is %f\n",row, col,matrix(row,col));
             
       }
      
}

long LoadOpenCLKernel(char const* path, char **buf)
{
	FILE  *fp;
	size_t fsz;
	long   off_end;
	int    rc;

	/* Open the file */
	fp = fopen(path, "r");
	if( NULL == fp ) {
		return -1L;
	}

	/* Seek to the end of the file */
	rc = fseek(fp, 0L, SEEK_END);
	if( 0 != rc ) {
		return -1L;
	}

	/* Byte offset to the end of the file (size) */
	if( 0 > (off_end = ftell(fp)) ) {
		return -1L;
	}
	fsz = (size_t)off_end;

	/* Allocate a buffer to hold the whole file */
	*buf = (char *) malloc( fsz+1);
	if( NULL == *buf ) {
		return -1L;
	}

	/* Rewind file pointer to start of file */
	rewind(fp);

	/* Slurp file into buffer */
	if( fsz != fread(*buf, 1, fsz, fp) ) {
		free(*buf);
		return -1L;
	}

	/* Close the file */
	if( EOF == fclose(fp) ) {
		free(*buf);
		return -1L;
	}


	/* Make sure the buffer is NUL-terminated, just in case */
	(*buf)[fsz] = '\0';

	/* Return the file size */
	return (long)fsz;
}

//int run_gauss_elim()
//int run_gauss_elim(float* k_vector, double* L_matrix, int width)  
int main( int argc, char *argv[] ) 
{

	/* Input Parsing */
	/* First input = square matrix dimensions (n) */
	/* Second input = local work size */
	unsigned long HA = atol(argv[1]);
	unsigned long WA = HA;
	int input_local_work_size = atoi(argv[2]);

	cout << "WA is of size: " << WA << "\n";
	cout << "local work size is of size: " << input_local_work_size << "\n";

	int err;                            // error code returned from api calls

	cl_device_id device_id;             // compute device id 
	cl_context context;                 // compute context
	cl_command_queue commands;          // compute command queue
	cl_program program;                 // compute program
	cl_kernel kernel1;                   // compute kernel
	cl_kernel kernel2;                   // compute kernel

	// OpenCL device memory for matrices
	cl_mem d_A;
        cl_mem d_vec_A;
        cl_mem diag_A;

	// set seed for rand()
 //	srand(2014);

	//Allocate host memory for matrices A and B
	unsigned long size_A = WA * HA;
	printf("size_A is %lu", size_A);
	unsigned long mem_size_A = sizeof(float) * size_A;

        int result_vector_size = sizeof(float)*HA;

        // Create eigen matrix
        int eigW = (int)HA;
        Eigen::MatrixXd eigen_matrix(eigW,eigW);
        // Create eigen vector
        Eigen::VectorXd eigen_vector(eigW);
       // std::cout << "The matrix m is of size " << eigen_matrix.rows() << "x" << eigen_matrix.cols() << std::endl;

	// printf("Initializing OpenCL device...\n"); 

	cl_uint dev_cnt = 0;
	clGetPlatformIDs(0, 0, &dev_cnt);

	cl_platform_id platform_ids[100];
	clGetPlatformIDs(dev_cnt, platform_ids, NULL);

	// Connect to a compute device
	int gpu = 1;
	err = clGetDeviceIDs(platform_ids[0], gpu ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU, 1, &device_id, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to create a device group!\n");
		return EXIT_FAILURE;
	}

	// Create a compute context 
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	if (!context)
	{
		printf("Error: Failed to create a compute context!\n");
		return EXIT_FAILURE;
	}

	// Create a command commands
	commands = clCreateCommandQueue(context, device_id, 0, &err);
	if (!commands)
	{
		printf("Error: Failed to create a command commands!\n");
		return EXIT_FAILURE;
	}

	// Create the compute program from the source file
	char *KernelSource;
	long lFileSize;

	// lFileSize = LoadOpenCLKernel("/home/s1350924/honproj/honproj/src/gauss_methods_opencl/gauss_elim_parallel/gaussian_elim_kernel.cl", &KernelSource);
	lFileSize = LoadOpenCLKernel("gaussian_elim_kernel.cl", &KernelSource);
	if( lFileSize < 0L ) {
		perror("File read failed");
		return 1;
	}

	program = clCreateProgramWithSource(context, 1, (const char **) & KernelSource, NULL, &err);
	if (!program)
	{
		printf("Error: Failed to create compute program!\n");
		return EXIT_FAILURE;
	}

	// Build the program executable
	err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		size_t len;
		char buffer[2048];
		printf("Error: Failed to build program executable!\n");
		printf("%d", clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len));
                for(int i = 0 ; i < 2048 ; i ++ ){
                    cout << buffer[i] ;//Looping 5 times to print out [0],[1],[2],[3],[4]
                } 
		exit(1);
	}


	// Create the input and output arrays in device memory for our calculation
	// That paper said to use CL_MEM_ALLOC_HOST_PTR.......dunno it it's the right choice
	d_A = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, mem_size_A, NULL, &err);
	if (!d_A)
	{
		printf("Error: Failed to allocate device memory!\n");
		exit(1);
	}    

        d_vec_A = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, result_vector_size, NULL, &err);
	if (!d_vec_A)
	{
		printf("Error: Failed to allocate device memory!\n");
		exit(1);
	}    

        diag_A = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, result_vector_size, NULL, &err);
	if (!d_vec_A)
	{
		printf("Error: Failed to allocate device memory!\n");
		exit(1);
	}    

	// http://stackoverflow.com/questions/17775738/cl-mem-alloc-host-ptr-slower-than-cl-mem-use-host-ptr
	cl_float* h_A = (cl_float*)clEnqueueMapBuffer(commands, d_A, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, mem_size_A, 0, NULL, NULL, &err); 
	cl_float* res_A = (cl_float*)clEnqueueMapBuffer(commands, d_vec_A, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, result_vector_size, 0, NULL, NULL, &err); 
	cl_float* diag_buffer = (cl_float*)clEnqueueMapBuffer(commands, diag_A, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, result_vector_size, 0, NULL, NULL, &err); 
        

	if(err){
		printf("You've got el error! %d", err);
	}

	//Initialize host memory
	randomMemInit(h_A, res_A, diag_buffer, eigen_matrix, eigen_vector, (int)WA, size_A); 

	// Unmap the buffer
	clEnqueueUnmapMemObject(commands, d_A, h_A, 0, NULL, NULL);
	clEnqueueUnmapMemObject(commands, d_vec_A, res_A, 0, NULL, NULL);
	clEnqueueUnmapMemObject(commands, diag_A, diag_buffer, 0, NULL, NULL);

	//Launch OpenCL kernel
	size_t localWorkSize[1], globalWorkSize[1];

	int wA = WA;

	// Set local and global work sizes for kernel 1
	localWorkSize[0] = input_local_work_size;
	unsigned long globSize = ((1+HA)*HA)/2+HA;   // The number of elements in the triangular matrix, + HA (the number of elements in the result vector k)
	int shader_cores = 4;
	int multiplicand = shader_cores * input_local_work_size; // Multiply by a constant??? 4 or 8
	if(globSize%multiplicand != 0){
		globSize = globSize-(globSize%multiplicand)+multiplicand; // Round up globSize to the nearest number that divides by local work size
	}
	globalWorkSize[0] = globSize;

	cout << "Global work size for kernel 1 is of size: " << globSize << "\n";





	// BEGIN GAUSSIAN ELIMINATION

	// Create the compute kernel in the program we wish to run
	kernel1 = clCreateKernel(program, "step_1_row_operation", &err);
	if (!kernel1 || err != CL_SUCCESS)
	{
		printf("Error: Failed to create compute kernel!\n");
		exit(1);
	}

	kernel2 = clCreateKernel(program, "step_2_col_operation", &err);
	if (!kernel2 || err != CL_SUCCESS)
	{
		printf("Error: Failed to create compute kernel!\n");
		exit(1);
	}
  


	/** QUERY LOCAL MEMORY SIZE **/
	cl_ulong local_mem_size;
	clGetDeviceInfo(device_id, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(local_mem_size), &local_mem_size, NULL);
	printf("Local memory size is: %lu \n", (unsigned long)local_mem_size);


	/** QUERY GLOBAL MEMORY SIZE **/
	cl_ulong global_mem_size;
	clGetDeviceInfo(device_id, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(global_mem_size), &global_mem_size, NULL);
	printf("Global memory size is: %lu \n", (unsigned long)global_mem_size);



	/** QUERY WORKGROUP SIZE FOR KERNEL 1 **/
	size_t workgroup_size;
	err = clGetKernelWorkGroupInfo(kernel1, device_id, CL_KERNEL_WORK_GROUP_SIZE,
			sizeof(size_t), &workgroup_size, NULL);
	if (err != CL_SUCCESS) { printf("Unable to get kernel1 work-group size"); }
	printf("Kernel1 work group size is: %d\n", workgroup_size);


	/** QUERY WORKGROUP SIZE FOR KERNEL 2 **/
	err = clGetKernelWorkGroupInfo(kernel2, device_id, CL_KERNEL_WORK_GROUP_SIZE,
			sizeof(size_t), &workgroup_size, NULL);
	if (err != CL_SUCCESS) { printf("Unable to get kernel2 work-group size"); }
	printf("Kernel2 work group size is: %d\n", workgroup_size);


	/*
	 * Query the device to find out it's prefered float vector width.
	 * Although we are only printing the value here, it can be used to select between
	 * different versions of a kernel.
	 */
	/* 
	   cl_float doubleVectorWidth;
	   clGetDeviceInfo(device_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, sizeof(cl_float), &doubleVectorWidth, NULL);
	   printf("Prefered vector width for floats: %d", doubleVectorWidth);
	 */
/*
           printf("\n\nMatrix A (Starting matrix)\n");
           for(unsigned long i = 0; i < size_A; i++)
           {
           printf("%f ", h_A[i]);

        // Add in new lines
        if(((i+1) % WA) == 0){
           printf("%f  !", res_A[i/WA]);
	   printf("\n");
        }
        }
        printf("\n");
        printf("\n");

      printf("Eigen matrix");
      for(int i = 0; i < HA; i++){
         for(int j = 0; j < HA; j++){
 
              printf("%f ",eigen_matrix(i,j));
          }
          printf("\n");
      }

*/
      // Arrays used in scaled partial pivoting
      float* row_maxes = (float*) malloc(sizeof(double)*HA);
      float* ratio_vector = (float*) malloc(sizeof(double)*HA);




                /**************************************/
                /* PIVOTING to prevent divide by zero */
                /**************************************/

                // find the pivot value, k
		/*
                if(h_A[currentRow*wA+currentRow] == 0){
                        int k;               
			for(int i = 0; i < HA; i++){
			  if(h_A[i*wA+currentRow] != 0){
			    k = i; 
                            break;
			  }
			}

			for(int col = currentRow; col < WA; col++){
			  // matrix[j][i] = matrix[j][i] + matrix[k][i];
			  h_A[currentRow*wA+col] = h_A[currentRow*wA+col] + h_A[k*wA+currentRow];
			}
                }
               */ 
                // End pivoting to prevent divide by zero 

                /************************/
                /*** Partial Pivoting ***/
                /************************/

                // compare the diagonal element of the next column with all the entries below it
                /*
                float diag = h_A[currentRow*WA+currentRow];
                int swapRow = -1;
                for(int i = currentRow; i < HA; i++){
                   if(abs(diag) < abs(h_A[i*WA + currentRow])){
                      swapRow = i;
                      diag = h_A[i*WA + currentRow];
                   }
                }

                // printf("Swapping row %d for %d \n", currentRow, swapRow);
                if(swapRow != -1){
                  //  printf("SWAP \n");
			for (int i = 0; i < WA; i++){
			   float inter = h_A[currentRow*wA+i];
			   h_A[currentRow*wA+i] = h_A[swapRow*wA+i]; 
			   h_A[swapRow*wA+i] = inter;
			}
                }
              */
              // End partial pivoting

              /***************************/
              /* Scaled partial pivoting */
              /***************************/

              // Create vector with largest values in each row 
              /*
              for(int cR = currentRow; cR < HA; cR++){
                 for(int i = currentRow; i < WA-1; i++){
                     if(i == currentRow){
                        row_maxes[cR] = fabs(h_A[cR*WA+i]);
                     }
                     else{
                        if( fabs(h_A[cR*WA+i]) > row_maxes[cR]){
                           row_maxes[cR] = fabs(h_A[cR*WA+i]);
                        }
                     }
                 } 
              } 


               // Set the ratio vector
		for(int i = 0; i < HA; i++){
                    int el_col = currentRow;
                    if(row_maxes[i] == 0 || h_A[i*WA+el_col] == 0)
                        ratio_vector[i] = 0;
                    else
                      ratio_vector[i] = abs(h_A[i*WA+el_col])/row_maxes[i];
                 }

             // Choose the largest number in the ratio vector, and switch the row at currentRow with the row at the index of that value in the ratio vector
               int max_ratio_index = 0;
               for(int i = 0; i < HA; i++){
                    if(ratio_vector[i] > ratio_vector[max_ratio_index])
                        max_ratio_index = i;
                 }

              // Swap rows

              if(max_ratio_index != currentRow){
                      for (int i = currentRow; i < WA; i++){
                           float inter = h_A[currentRow*wA+i];
                           h_A[currentRow*wA+i] = h_A[max_ratio_index*wA+i]; 
                           h_A[max_ratio_index*wA+i] = inter;
                        }

              }
             */
            // End scaled partial pivoting



		/************/
		/* Kernel 1 */
		/************/
		// Make diagonal component == 1 by dividing all non-zero elements by it.

		// Setting arguments for the kernel
		err = clSetKernelArg(kernel1, 0, sizeof(cl_mem), (void *)&d_A);
                err |= clSetKernelArg(kernel1, 1, sizeof(cl_mem), (void *)&d_vec_A); 
                err |= clSetKernelArg(kernel1, 2, sizeof(cl_mem), (void *) &diag_A);
		err |= clSetKernelArg(kernel1, 3,sizeof(cl_int), (void *)&wA);

		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to set kernel arguments for kernel 1! %d\n", err);
			exit(1);
		}
                
                float* diag_debug = (float*)malloc(sizeof(float)*WA);
                printf("Diag elements before kernel 1: \n");
                for(int i = 0; i < WA; i++){
                    diag_debug[i] = diag_buffer[i];
		    printf("%f ", diag_debug[i]);
                }
                printf("\n");
		

		// This is where execution of the kernel is actually done
		err = clEnqueueNDRangeKernel(commands, kernel1, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);

		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to execute kernel1! %d\n", err);
			exit(1);
		}
                
                

		// Answer #2: 
		// http://stackoverflow.com/questions/26569762/optimising-host-to-gpu-transfer
		h_A = (cl_float*)clEnqueueMapBuffer(commands, d_A, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, mem_size_A, 0, NULL, NULL, &err);
	        res_A = (cl_float*)clEnqueueMapBuffer(commands, d_vec_A, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, result_vector_size, 0, NULL, NULL, &err); 

                diag_buffer = (cl_float*)clEnqueueMapBuffer(commands, diag_A, CL_TRUE, CL_MAP_READ, 0, result_vector_size, 0, NULL, NULL, &err);


		//Retrieve result from device
		clEnqueueUnmapMemObject(commands, d_A, h_A, 0, NULL, NULL);       
	        clEnqueueUnmapMemObject(commands, d_vec_A, res_A, 0, NULL, NULL);
	        clEnqueueUnmapMemObject(commands, diag_A, diag_buffer, 0, NULL, NULL);

                 // Check diag elements
                for(int i = 0; i < WA; i++){
		    printf("%f ", diag_buffer[i]);
                    if(diag_buffer[i] != diag_debug[i]){
                          printf("ERROR: DIAG ELEMENTS NOT THE SAME!!!!\n");
                          printf("ERROR: DIAG ELEMENTS NOT THE SAME!!!!\n");
                          printf("ERROR: DIAG ELEMENTS NOT THE SAME!!!!\n");
                          printf("ERROR: DIAG ELEMENTS NOT THE SAME!!!!\n");
                          printf("ERROR: DIAG ELEMENTS NOT THE SAME!!!!\n");
                          printf("ERROR: DIAG ELEMENTS NOT THE SAME!!!!\n");
                    }
                }
                free(diag_debug);


           printf("\n\nMatrix A (Starting matrix)\n");
           for(int i = 0; i < size_A; i++)
           {
           printf("%f ", h_A[i]);

        // Add in new lines
        if(((i+1) % WA) == 0){
           printf("%f  !", res_A[i/WA]);
           printf("\n");
        }
        }
        printf("\n");
        printf("\n");

              
		/************/
		/* Kernel 2 */
		/************/
                
	//Launch OpenCL kernel
	for(int currentCol = 0; currentCol < WA; currentCol++){

		// Set local and global work sizes for kernel 2 
		size_t localWorkSize2[1], globalWorkSize2[1];
		globSize = HA-currentCol;
		if(globSize%multiplicand != 0){
			globSize = globSize-(globSize%multiplicand)+multiplicand; // Round up globSize to the nearest number that divides by local work size
		}
        
		localWorkSize2[0] = input_local_work_size;
		globalWorkSize2[0] = globSize;

		cout << "Global work size for kernel 2 is of size: " << globSize << "\n";


		err = clSetKernelArg(kernel2, 0, sizeof(cl_mem), (void *)&d_A);
                err |= clSetKernelArg(kernel2, 1, sizeof(cl_mem), (void *)&d_vec_A); 
		err |= clSetKernelArg(kernel2, 2, sizeof(int), (void *)&wA);
		err |= clSetKernelArg(kernel2, 3, sizeof(int), (void *)&currentCol);

		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to set kernel arguments! %d\n", err);
			exit(1);
		}

		//print out the results
/*
		   printf("\n\nMatrix A (before kernel) for col %d\n", currentCol);
		   for(unsigned long i = currentCol; i < wA; i++)
		   {
		   printf("k: %f - %f ",res_A[i], h_A[i*wA+currentCol]);

		// Add in new lines
		if(((i + 1) % WA) == 0){
		printf("\n");
		}
		}
		printf("\n");
*/
		// This is where execution of the kernel is actually done
		err = clEnqueueNDRangeKernel(commands, kernel2, 1, NULL, globalWorkSize2, localWorkSize2, 0, NULL, NULL);

		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to execute kernel2! %d\n", err);
			exit(1);
		}

		h_A = (cl_float*)clEnqueueMapBuffer(commands, d_A, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, mem_size_A, 0, NULL, NULL, &err);
	        res_A = (cl_float*)clEnqueueMapBuffer(commands, d_vec_A, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, result_vector_size, 0, NULL, NULL, &err); 

		clEnqueueUnmapMemObject(commands, d_A, h_A, 0, NULL, NULL);    
	        clEnqueueUnmapMemObject(commands, d_vec_A, res_A, 0, NULL, NULL);

/*
		//print out the results
		   printf("\n\nMatrix A (aftiesssss kernel) for col %d\n", currentCol);
		   for(unsigned long i = currentCol; i < wA; i++)
		   {
		   printf("%f, ",res_A[i]);
		}
		printf("\n");
*/
	 }



	// Checking if the result is correct
/*
	unsigned long row_count = 0;
	for(unsigned long i = 0; i < size_A; i++)
	{
		if(((i + 1) % WA) == 0){
			if(h_A[i] == 0){
				printf("\n\n ERROR! INCORRECT RESULT result should be some integer: %f, %lu \n\n", h_A[i], i);
				//     break;
			}
		}else if(i%WA == row_count){
			if(h_A[i] != 1.0){
				printf("\n\n ERROR! INCORRECT RESULT diag should be 1: %f, %lu \n\n", h_A[i], i);
				//     break;
			}
		}else{
			if(h_A[i] != 0){
				printf("\n\n ERROR! INCORRECT RESULT should be zero: %f, %lu \n\n", h_A[i], i);
				//      break;
			}
		} 

		// At every newline, increase the row_count
		if(((i + 1) % WA) == 0){
			row_count++;
		}

	}
	printf("\n");
*/

 

       // Do Eigen linear equation solving
       // eigen_matrix.topLeftCorner(HA,HA).triangularView<Eigen::Lower>().solveInPlace(eigen_vector);
       eigen_matrix.triangularView<Eigen::Lower>().solveInPlace(eigen_vector);

       // Match output of parallel implementation to the output of the Eigen operation
       // Produce a statistic for mean error




/*
      printf("Eigen matrix");
      for(int i = 0; i < HA; i++){
         for(int j = 0; j < HA; j++){

              printf("%f ",eigen_matrix(i,j));
          }
          printf("\n");
      }
*/




      
       printf("Solution vector of OpenCL: \n");
       for(int i = 0; i < HA; i++){
	  printf("%f, ", res_A[i]);
       } 

       printf("Solution vector of Eigen: \n");
       double mean_difference; 
       for(int i = 0; i < HA; i++){
          float eig_val = eigen_vector(i);
          printf("%f, ", eig_val);
          float opencl_val = res_A[i];
          mean_difference += fabs(fabs(eig_val) - fabs(opencl_val));
       } 
       mean_difference = mean_difference/HA;
       printf("\n");

       printf("Mean difference between eig and opencl is: %f \n", mean_difference);






	//Shutdown and cleanup

	// Unmap the buffer after all is said and done
	clEnqueueUnmapMemObject(commands, d_A, h_A, 0, NULL, NULL);

	clReleaseMemObject(d_A);

	clReleaseProgram(program);
	clReleaseKernel(kernel1);
	clReleaseKernel(kernel2);
	clReleaseCommandQueue(commands);
	clReleaseContext(context);

 	free(row_maxes); 
        free(ratio_vector);


	return 0;
}
