export OPENCL_PATH=/usr/local/lib/mali/fbdev && export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$OPENCL_PATH && g++ -I ../../../Eigen -o gaussian_elim gaussian_elim_host.cpp -I/usr/include -L$OPENCL_PATH -Wl,-rpath,$OPENCL_PATH -lOpenCL && ./gaussian_elim 20 7

# Search for (-l)OpenCL in directory specifed by the path (-L)$OPENCL_PATH.
# -Wl option .Pass option as an option to the linker. If option contains commas, it is split into multiple options at the commas.
