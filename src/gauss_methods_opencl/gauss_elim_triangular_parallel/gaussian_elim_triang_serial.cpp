#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <CL/cl.h>
#include <stdbool.h>
#include <math.h>
#include <iostream>
#include <Eigen>
using namespace Eigen;
using namespace std;


int triangSerial(double* k_in, double* L_in, int n){

        // int n = 600;

        double* matrix_arr = (double*)malloc(sizeof(double)*n*n);
        double* vector_arr = (double*)malloc(sizeof(double)*n);
        double* diags = (double*)malloc(sizeof(double)*n);
        
        Eigen::MatrixXd eigen_matrix(n,n);
        Eigen::VectorXd eigen_vector(n);

        int fMin = 0;
        int fMax = 1.5;

        int needed_width = 0;
        for (int i = 0; i < n*n; ++i){
                   matrix_arr[i] = L_in[i];
                   int row = i/n;
                   int col = i%n;
                   eigen_matrix(row,col) = L_in[i];
          /*
                   int row = i/n;
                   int col = i%n;
                   double f = rand() / (float)RAND_MAX;
                if(col <= needed_width){
                        // random double uniformly distributed between 0 and 1
                        matrix_arr[i] = fMin + f * (fMax - fMin);
                }else{
                        matrix_arr[i] = 0;
                }

                // Set diagonal matrix values
                if(row == col){
                    diags[row] = fMin + f * (fMax - fMin);
               }

                // Set random vector values
                if(col == n-1){
                   needed_width++;
                   double ra = rand() / (float)RAND_MAX;
                   vector_arr[row] = fMin + ra * (fMax - fMin);
                   printf(" %f \n ", vector_arr[row]);
                   eigen_vector(row) = fMin + ra * (fMax - fMin);
                }

               // Assign values for the Eigen matrix
               eigen_matrix(row,col) = fMin + f * (fMax - fMin);
               printf("matrix(%d,%d) is %f\n",row, col,eigen_matrix(row,col));
         */

       }

       for(int i = 0; i < n; i++){
            vector_arr[i] = k_in[i];
            eigen_vector[i] = k_in[i];
       }


       for(int i = 0; i < n*n; i++){
	     int row = i/n;
	     int col = i%n;
	     matrix_arr[row*n+col] = matrix_arr[row*n+col]/diags[row];
       }

       for(int i = 0; i < n; i++){
	       vector_arr[i] = vector_arr[i]/diags[i];
       }


       printf("\n\nMatrix A (Starting matrix)\n");
           for(unsigned long i = 0; i < n*n; i++)
           {
           printf("%f ", matrix_arr[i]);

        // Add in new lines
        if(((i+1) % n) == 0){
           printf("%f  !", vector_arr[i/n]);
           printf("\n");
        }
        }
        printf("\n");
        printf("\n");



       for(int currentCol = 0; currentCol < n; currentCol++){
	       for(int i = currentCol+1; i < n; i++){
		     vector_arr[i] = vector_arr[i]-matrix_arr[i*n+currentCol]*vector_arr[currentCol];
	       }
       }
      /*
       printf("Final vector array solution \n");
       for(int i = 0; i < n; i++){
           printf("%f, ", vector_arr[i]);
          }
       printf("\n \n");
*/
       eigen_matrix.triangularView<Eigen::Lower>().solveInPlace(eigen_vector);

       double mean_difference; 
       for(int i = 0; i < n; i++){
          double eig_val = eigen_vector(i);
          printf("%f, ", eig_val);
          double opencl_val = vector_arr[i];
          mean_difference += fabs(fabs(eig_val) - fabs(opencl_val));
       } 
       mean_difference = mean_difference/n;
       printf("\n");

       printf("Mean difference between eig and serial triangular solve is: %f \n", mean_difference);


}

