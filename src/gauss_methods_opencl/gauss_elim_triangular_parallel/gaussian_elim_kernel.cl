/* kernel.cl 
 * Matrix multiplication: C = A * B.
 * Device code.
 */
 
// OpenCL Kernel

// Enables float precision doubleing point operations
// #pragma OPENCL EXTENSION cl_khr_fp64 : enable

__kernel void step_1_row_operation(__global float* A, __global float* k, __global float* diags, int wA )
{

    /* KERNEL #1 */
    // Do step 1 for each element of the current row: R_i = R_i/a_ii

   int index = get_global_id(0); 
   // http://stackoverflow.com/questions/9674179/getting-the-row-and-column-of-a-triangular-matrix-given-the-index


   if(index < wA){
     k[index] = k[index]/diags[index];
   }
   else if(index >= wA && index < wA+((wA+1)*(wA))/2){ 
    // A[currentRow][tx] = A[currentRow][tx]/ a;
    // A[currentRow*wA+col] = A[currentRow*wA+col]/A[currentRow*wA+currentRow];
     index = index-wA;
     int row = floor(-0.5 + sqrt(0.25 + 2 * index));
     int triangularNumber = row * (row + 1) / 2;
     int col = index - triangularNumber;
     A[row*wA+col] = A[row*wA+col]/diags[row];
   }
}


__kernel void step_2_col_operation(__global float* A, __global float* k, int wA, int currentCol)
{
  
   int row = get_global_id(0)+currentCol+1;

   int hA = wA;

   // int row = floor(-0.5 + sqrt(0.25 + 2 * i)) + currentCol;
   // int triangularNumber = row * (row + 1) / 2;
   // int col = index - triangularNumber;

  // Condition 1: as long as we haven't overrun the size of the matrix
  if(row<hA ){ 

    /** KERNEL #2 **/
    k[row] = k[row]-A[row*wA+currentCol]*k[currentCol];
  }
}



 
/*
__kernel void step_3_zero_col(__global float* A, int wA, int currentRow)
{

   int row = get_global_id(0);

   int hA = wA-1;

  if(row != currentRow && row<hA){
    A[row*wA + currentRow] = 0;
  }
}
*/
