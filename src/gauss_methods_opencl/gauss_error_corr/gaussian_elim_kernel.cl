/* kernel.cl 
 * Matrix multiplication: C = A * B.
 * Device code.
 */
 
// OpenCL Kernel

// Enables float precision doubleing point operations
// #pragma OPENCL EXTENSION cl_khr_fp64 : enable

__kernel void step_1_row_operation(__global float* A, float aii, int wA, int currentRow)
{

    /* KERNEL #1 */
    // Do step 1 for each element of the current row: R_i = R_i/a_ii

   int col = get_global_id(0); 

   if(col < wA){ 
    // A[currentRow][tx] = A[currentRow][tx]/ a;
    // A[currentRow*wA+col] = A[currentRow*wA+col]/A[currentRow*wA+currentRow];
      A[currentRow*wA+col] = A[currentRow*wA+col]/aii;
  }
 
}


__kernel void step_2_col_operation(__global float* A, int wA, int currentRow)
{
  
   int i = get_global_id(0);

   int hA = wA-1;

   int row = i/wA;
   int col = i%wA;

  // Condition 1: Don't operate on the row operated on in Kernel 1
  // Condition 2: as long as we haven't overrun the size of the matrix
  // Condition 3: Don't operate on R_j
  if(row != currentRow && i<(wA*hA) && (i != row*wA+currentRow)   ){ 

    /** KERNEL #2 **/
    // n^2 loop for Step 2

    // A[row][col]  = A[row][col] - A[row][currentRow] * A[currentRow][col];

    // float R_j = A[row*wA + currentRow];
    // A[row*wA + col]  = A[row*wA + col] - R_j * A[currentRow*wA + col];
     
    float inter = A[i] - A[row*wA + currentRow] * A[currentRow*wA + col]; 
    A[i] = inter;
  }
}
