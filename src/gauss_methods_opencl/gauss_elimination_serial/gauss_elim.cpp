#include <iostream>
#include <algorithm>
#include <iterator>
#include <stdio.h>
#include <math.h>

using namespace std;

// Allocates a matrix with random float entries.
void randomMemInit(float* data, int size)
{
  int i;

  for (i = 0; i < size; ++i)
    data[i] = rand() / (float)RAND_MAX;

}


int main(int argc, char *argv[])
{
  int HA = atoi(argv[1]);
  int WA = HA+1;

  int size_A = WA * HA;
  int mem_size_A = sizeof(float) * size_A;
  float* orig_mat = (float*) malloc(mem_size_A);
  randomMemInit(orig_mat,size_A);

  // Print the starting matrix
   printf("\n\nMatrix A (Original matrix)\n");
  for(unsigned long i = 0; i < size_A; i++)
  {
    printf("%f ", orig_mat[i]);

    // Add in new lines
    if(((i + 1) % WA) == 0){
      printf("\n");
    }
  }
  printf("\n");
 
   


  /********************************************/
  /***        Gaussian elimination          ***/
  /********************************************/

  // Loop through rows for Step 1
  for (int currentRow = 0; currentRow < HA; currentRow++)
  {
    /** KERNEL #1 **/
    float a = orig_mat[currentRow*WA+currentRow];
    printf("Diag is: %f \n", a);
    // Do step 1 for each element of the current row: R_i = R_i/a_ii
    for(int col = 0; col < WA; col++){
      orig_mat[currentRow*WA+col] = orig_mat[currentRow*WA+col]/ a;
    }

    /** KERNEL #2 **/
    // n^2 loop for Step 2
    for (int row = 0; row < HA; row++)
    {
      // Skip the row if step 1 has already been applied to it
    float R_j = orig_mat[row*WA+currentRow];
    for (int col = 0; col < WA; col++)
      {
	if(row != currentRow)
	{	
	  // std::cout << orig_mat[row][col] << " - " << R_j << " * " << orig_mat[currentRow][col] << std::endl;
	  orig_mat[row*WA+col]  = orig_mat[row*WA+col] - R_j * orig_mat[currentRow*WA+col];
	}
      }
    }
  }




  // store solution vector as an array
  /*
     float solution_vector[n];
     for (int i = 0; i < n; i++) {
     solution_vector[i] = orig_mat[i][n];
     }	
   */


  // Checking if the result is correct
  unsigned long row_count = 0;
  for(unsigned long i = 0; i < size_A; i++)
  {
    if(((i + 1) % WA) == 0){
      if(orig_mat[i] == 0){
	printf("\n\n ERROR! INCORRECT RESULT result should be some integer: %f, %lu \n\n", orig_mat[i], i);
	//     break;
      }
    }else if(i%WA == row_count){
      if(orig_mat[i] != 1.0){
	printf("\n\n ERROR! INCORRECT RESULT diag should be 1: %f, %lu \n\n", orig_mat[i], i);
	//     break;
      }
    }else{
      if(orig_mat[i] != 0){
	printf("\n\n ERROR! INCORRECT RESULT should be zero: %f, %lu \n\n", orig_mat[i], i);
	//      break;
      }
    } 

    // At every newline, increase the row_count
    if(((i + 1) % WA) == 0){
      row_count++;
    }

  }
  printf("\n");



  //print out the results
  
  printf("\n\nMatrix A (Results)\n");
  for(unsigned long i = 0; i < size_A; i++)
  {
    printf("%f ", orig_mat[i]);

    // Add in new lines
    if(((i + 1) % WA) == 0){
      printf("\n");
    }
  }
  printf("\n");
  

  //Print the solution vector
  /*
     std::cout <<  "Solution vector: "<< std::endl;
     for (int i = 0; i < n; i++) {
     std::cout <<  orig_mat[i][n] << ", "<< ' ';
     }	
     std::cout << std::endl;
   */

  // Deallocate array memory
  free(orig_mat);

  return 0;
}
