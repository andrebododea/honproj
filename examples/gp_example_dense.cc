// libgp - Gaussian process library for Machine Learning
// Copyright (c) 2013, Manuel Blum <mblum@informatik.uni-freiburg.de>
// All rights reserved.

#include "gp.h"
#include "gp_utils.h"
#include <ctime>


#include <Eigen/Dense>

using namespace libgp;

int main (int argc, char const *argv[])
{
// Original size
  int n=10000, m=1000;
  
// larger matrix size
//  int n=8000, m=2000; 
  double tss = 0, error, f, y;
  // initialize Gaussian process for 2-D input using the squared exponential 
  // covariance function with additive white noise.
  GaussianProcess gp(2, "CovSum ( CovSEiso, CovNoise)");
  // initialize hyper parameter vector
  Eigen::VectorXd params(gp.covf().get_param_dim());
  // Initializes params vector of <<0.0, 0.0, -2.0>> to be used as parameters of covariance function
  params << 0.0, 0.0, -2.0;
  // set parameters of covariance function
  gp.covf().set_loghyper(params);

  // add training patterns
    // (makes training data out of our n points with uniformly distributed pseudorandom numbers, and adds it to the gp)

  double time = 0.0; // timing code ANDRE
 
  for(int i = 0; i < n; ++i) {
    double x[] = {drand48()*4-2, drand48()*4-2};
    y = Utils::hill(x[0], x[1]) + Utils::randn() * 0.1;
    //time_t start = time(0);  // timing code ANDRE
    gp.add_pattern(x, y);
  // time_t end = time(0); // timing code ANDRE
  // time += difftime(end, start) * 1000.0;  // timing code ANDRE
  }


  std::cout << "add_pattern() took " << time << " seconds." << std::endl;  



  // total squared error
  // With f(x) being the modeled function based on the regression
  for(int i = 0; i < m; ++i) {
    double x[] = {drand48()*4-2, drand48()*4-2};
    f = gp.f(x);
    y = Utils::hill(x[0], x[1]);
    error = f - y;
    tss += error*error;
  }

  std::cout << "mse = " << tss/m << std::endl;
	return EXIT_SUCCESS;
}

