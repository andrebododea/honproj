#ifndef __GAUSS_ELIM_H__
#define __GAUSS_ELIM_H__

// int run_gauss_elim();
int triangSerial(double* k_pass_out, double* L_pass_out, int n);
int triangParallel(double* k_pass_out, double* L_pass_out, int n);
//int run_gauss_elim(double* k_pass_out, double* L_pass_out, int n);

#endif
