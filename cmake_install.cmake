# Install script for directory: /afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "0")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/gp" TYPE FILE FILES
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov_factory.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov_linear_ard.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov_linear_one.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov_matern3_iso.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov_matern5_iso.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov_noise.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov_rq_iso.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov_periodic_matern3_iso.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov_periodic.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov_se_ard.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov_se_iso.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov_sum.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cov_prod.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/gp.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/gp_utils.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/sampleset.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/rprop.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/input_dim_filter.h"
    "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/include/cg.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64" TYPE STATIC_LIBRARY FILES "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/libgp.a")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib64/pkgconfig" TYPE FILE FILES "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/libgp.pc")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/examples/cmake_install.cmake")
  INCLUDE("/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/tests/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

IF(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
ELSE(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
ENDIF(CMAKE_INSTALL_COMPONENT)

FILE(WRITE "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/${CMAKE_INSTALL_MANIFEST}" "")
FOREACH(file ${CMAKE_INSTALL_MANIFEST_FILES})
  FILE(APPEND "/afs/inf.ed.ac.uk/user/s13/s1350924/Downloads/hons_proj_jan_23/honproj/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
ENDFOREACH(file)
